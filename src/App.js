/* ##########################################################################################

This is the top component, setting up all the routing needed.

########################################################################################## */


import React from 'react';
import './App.css';
import Characters from "./components/Characters/Characters"
import CharacterDetails from "./components/CharacterDetails/CharacterDetails";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/character/:id" component={CharacterDetails}/>
          <Route path="/">
            <Characters />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
