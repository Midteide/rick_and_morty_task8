/* ##########################################################################################

This component prints the details of the characters pushed in the main component (Characters).
The character to be printed is determined by the address field (/character/<id>)

########################################################################################## */


import React, { useEffect, useState } from 'react' 
import Styles from "./CharacterDetails.module.css"
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'


const CharacterDetails = (props) =>
{
    const [character, setCharacter] = useState();
    
    useEffect(() => {
        let apiUrl = "https://rickandmortyapi.com/api/character/" + props.match.params.id;
        fetch(apiUrl)
            .then(resp => resp.json())
            .then(resp => {
                setCharacter(resp)
            })
            .catch(err => console.error(err));
    // eslint-disable-next-line
},[])
    
    const renderCharacter = () => {
        if (character) {
            return (
                <Row>
                    <Col>
                        <img className="z-depth-4" id={Styles.CharacterItemImg} src={character.image} alt={character.name} />
                    </Col>
                    <Col>
                    <p className={Styles.CharacterInfoText}><b>Name:</b> {character.name}</p>
                    {character.type && <p className={Styles.CharacterInfoText}><b>Type:</b> {character.type}</p>}
                    <p className={Styles.CharacterInfoText}><b>Status:</b> {character.status}</p>
                    <p className={Styles.CharacterInfoText}><b>Species:</b> {character.species}</p>
                    <p className={Styles.CharacterInfoText}><b>Gender:</b> {character.gender}</p>
                    <p className={Styles.CharacterInfoText}><b>Created:</b> {character.created}</p></Col>
                </Row>
            )
        }
    }

    return (
        <div className="CharacterDetails container">
            <Row>
                <h1>
                    {character && character.name}
                </h1>
            </Row>
            
            {renderCharacter()}

        </div>
    )
}

export default CharacterDetails;