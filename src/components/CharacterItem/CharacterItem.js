/* ##########################################################################################

This component takes a character object and prints the name and pictures to screen in a
card element. These will be responsive, printing 1, 2, 3, 4 or 6 cards depending on screen
size.

########################################################################################## */


import React from 'react';
import Styles from "./CharacterItem.module.css"
import Col from 'react-bootstrap/Col'

const CharacterItem = (props) => {
    const { character } = props;
    return (
        // Adding columnt for each character, with responsiveness for different screen sizes
        <Col xs={12} sm={6} md={4} lg={3} xl={2} className={Styles.CharacterItem} key={character.id}>
            <div className="card z-depth-3" id={Styles.CharacterItem}>
                <a href={`/character/${character.id}`}>
                    <div className="card-image" >
                        <img src={character.image} className="z-depth-4" id={Styles.CharacterItemImg} alt={character.name} /> 
                    </div>
                    <div className="card-content" id={Styles.cardFixedHeight}>
                        <p>{character.name}</p>
                    </div>
                </a>
            </div>
        </Col>
    )
}

export default CharacterItem;