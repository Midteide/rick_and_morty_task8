/* ##########################################################################################

This is the main component and will fetch 20 objects and print them. If user scrolls
to bottom of the page then 20 more objects will be loaded.

This component also does the search handling, taking the reference to the searchbar from
the SearchBar component and filtering the existing (already fetched) items accordingly.

########################################################################################## */


import React, { useState, useEffect, useRef} from "react";
import CharacterItem from "../CharacterItem/CharacterItem"
import SearchBar from "../SearchBar/SearchBar.js"
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'


const Characters = () => {
    const [characters, setCharacters] = useState([]);
    const [searchTerm, setSearchTerm] = useState(null)
    const [getMoreCharacters, setGetMoreCharacters] = useState(true);
    const [nextPageUrl, setNextPageUrl] = useState("https://rickandmortyapi.com/api/character/");
    let searchInputRef = useRef(null);

    // Adding functionality for automatic fetching of more characters when user scrolls
    // to bottom of the page
    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    function handleScroll() {
        if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight) return;
        setGetMoreCharacters(true);
    }

    // Fetch more characters if "getMoreCharacters"-flag is set
    useEffect(() => {
        if (getMoreCharacters){
            fetch(nextPageUrl)
            .then(resp => resp.json())
            .then(resp => {
                setNextPageUrl(resp.info.next)
                const { results } = resp || [];
                let temp = [...characters, ...results]
                setCharacters(temp);
                setGetMoreCharacters(false);
            })
            .catch(err => console.error(err));
        }
    // eslint-disable-next-line
    },[getMoreCharacters])

    // Triggered by search bare modifications
    const handleSearchBar = () => {
        // Make the searching not case sensitive by making search string all lowercase, 
        // same as is done with user name in the renderCharacters function
        setSearchTerm(searchInputRef.current.value.toLowerCase())
    }

    const renderCharacters = (props) => {
        let filteredCharacters = [];

    // Check if user has typed in a search term:
    if (searchTerm) {
        filteredCharacters = characters.filter(character => character.name.toLowerCase().includes(searchTerm))
        
        // Print results if any, if not print "No results."
        if (filteredCharacters.length) 
            return filteredCharacters.map(character => <CharacterItem character={character} />) 
        else 
            return (
                <>
                    <p>No results.</p>
                </>
            )
    }
    else  // if there is no search term entered, display all fetched characters
        return (
            <>
                {characters.map(character => <CharacterItem character={character} />)}
            </>
         )
    }

    return (
        <div>
            <Container>
                <Row>
                <Col>
                    <h1 className="text-center"> Rick and Morty </h1>
                    </Col>
                </Row>

                <Row>
                    <Col >
                        <SearchBar searchInputRef={searchInputRef} handleSearch={handleSearchBar} />
                    </Col>
                </Row>
                
                <Row>
                    { renderCharacters() }
                </Row>

            </Container> 
        </div>
    )

}

export default Characters;