/* ##########################################################################################

This component is responsible for taking search parameters from the user and forwarding
them to the parent component (Characters)

########################################################################################## */


import React from 'react'
import "./SearchBar.module.css"

const SearchBar = (props) => {

    const handleKeyPress = (e) => {
        props.handleSearch(e)
    }

    return (
        <div className="searchbar" >
            <h1>
                <input type="text" ref={props.searchInputRef} placeholder="Search for character..." onChange={ handleKeyPress }  ></input>
            </h1>
        </div>
    )
}

export default SearchBar;